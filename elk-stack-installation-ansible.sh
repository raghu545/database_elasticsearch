sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install ansible
ansible --version

add below lines in /etc/ansible/hosts

'[test]
mymachine ansible_connection=local'

Execute : ansible-playbook elk-config.yml

Test : 
elasticsearch- http://localhost:9200/
kibana- http://localhost:5601/
