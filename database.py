import psycopg2
import schedule
import time



def my_func():
    con = psycopg2.connect(
            host = "localhost",
            database="my_database",
            user = "postgres",
            password = "12345")

#cursor 
    cur = con.cursor()

    cur.execute("""CREATE TABLE IF NOT EXISTS contacts(
        uid integer,
        email character varying[],
        first_name character varying[],
        last_name character varying[]
    )
    """)
#execute query
#cur.execute("select uid, email,first_name, last_name from contacts")

#rows = cur.fetchall()

#for r in rows:
   # print (r)

    f = open('/home/raghuveera/contacts.csv', 'r')
    cur.copy_from(f, 'contacts', sep=',')
    f.close()
    print("I'm working...")


#commit the transcation 
    con.commit()

#close the cursor
    cur.close()

#close the connection
    con.close()

schedule.every(5).seconds.do(my_func)

while True:
    schedule.run_pending()
    time.sleep(1)