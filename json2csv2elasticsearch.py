
import json
import csv
import os
import time

directory_path = '/home/lenovo/log/tmp/'
json_path = '/home/lenovo/log/clusters.json'
csv_path ='/home/lenovo/log/file.csv'
boolean_false = 'false'
 
writer = csv.writer(open(csv_path, 'w'))
headers = ['Unique_id', 'Timestamp','boolean']
 
writer.writerow(headers)
 
with open(json_path) as json_file:
    data = json_file.read()
parsed_json = json.loads(data)

for key, value in parsed_json.items():
        for data, filename in zip(parsed_json[key], os.listdir(directory_path)):
            row = []
            row.append(str(key))
            file_path   = directory_path + filename
            created     = time.ctime(os.path.getctime(file_path))
            row.append(str(created))
            row.append(str(boolean_false))
            writer.writerow(row)